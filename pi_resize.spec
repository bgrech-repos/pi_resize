Name:           pi_resize
Version:        0.1
Release:        4%{?dist}
Summary:        Automatically resize your partition on first boot

License:        MIT
Source0:        pi_resize
Source1:        pi_resize.service
Source2:        pi_resize.preset

BuildArch:      noarch
BuildRequires:  systemd-rpm-macros
Requires:       cloud-utils-growpart

%description
Script and service to automatically resize the "/" partition on first boot of
your raspberry pi

%prep

%build

%install
mkdir -p %{buildroot}%{_libexecdir}
install -m 0755 %{SOURCE0} %{buildroot}%{_libexecdir}/pi_resize

mkdir -p %{buildroot}%{_unitdir}
install -m 0644 %{SOURCE1} %{buildroot}%{_unitdir}/pi_resize.service

mkdir -p %{buildroot}%{_presetdir}
install -m 0644 %{SOURCE2} %{buildroot}%{_presetdir}/50-pi_resize.preset

%post
%systemd_post pi_resize.service

%preun
%systemd_preun pi_resize.service

%postun
%systemd_postun pi_resize.service


%files
%{_libexecdir}/pi_resize
%{_unitdir}/pi_resize.service
%{_presetdir}/50-pi_resize.preset


%changelog
* Wed Oct 16 2024 Stephen Bertram <sbertram@redhat.com> - 0.1.4
- Add check to better figure out root partion on aboot devices

* Mon Feb 5 2024 bgrech <bgrech@redhat.com> - 0.1.3
- Add support for ostree systems with read only sysroot mount

* Tue May 03 2022 Pierre-Yves Chibon <pingou@pingoured.fr> - 0.1-2
- Add Requires to cloud-utils-growpart

* Tue May 03 2022 Pierre-Yves Chibon <pingou@pingoured.fr> - 0.1-1
- Initial version

